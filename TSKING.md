frontend
=====================
AC1 商城
1、进入商城页面，就返回所有商品的信息（api:get:products）
`GET：   /api/products `
response: 
[{id，
  name
  Price, 
  Unit, 
  Img,
  }]
2、添加订单：每个商品列表的item可以进行添加操作，每点击一次按钮，quantity增加1
`POST:   /api/order/{productId}`
CreateProductRequest:{name,price, unit}
查询上一次的order的quantity，quantity再++1
order:save new order（ name，
                       price, 
                       quantity, 
                       unit ）
3、添加操作没有接收到response之前需要禁用添加按钮样式，success之后启用按钮

=====================

AC2 订单页面
1、进入订单页面，就返回所有的订单信息（api:get:orders）
`GET：   /api/orders `
一个order对应一个product，
[id，productId，
  productName，
  price, 
  quantity, 
  unit
  }]
2、订单页面选择性渲染，当response的orders长度为0 时显示“暂无订单，返回商城页面继续购买”，否则显示订单列表
3、渲染订单列表中可以进行删除订单操作，更新订单列表，删除的一项消失，
`DELETE：   /api/orderid `
response: 200;
4、删除订单操作失败时弹出提示：商品删除失败，请稍后再试

=====================

AC3 添加商品页
1、添加商品表单，点击提交创建商品，返回商城页面，商品列表更新
`POST:   /api/product`
response:200
2、前端表单控制表单input不能为空，指定input类型是数字，否则禁止提交按钮
3、后端存商品的时候，先执行查询，判断名字是否存在，存在就返回错误消息：商品名称已存在，请输入新的商品名称

