package com.twuc.webApp;

import com.twuc.webApp.contracts.NewProductRequest;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest extends ApiTestBase {
    @Autowired
    ProductRepository productRepository;

    @Test
    void should_get_all_products() throws Exception {
        mockMvc.perform(get("/api/products"))
                .andExpect(status().is(200));
    }

    @Test
    void should_insert_new_product() throws Exception {
        mockMvc.perform(post("/api/product")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"title\":\"dfd\",\"price\":\"34\",\"unit\":\"fdf\",\"img\":\"fd\"}"))
                .andExpect(status().isOk());
    }
}
