CREATE TABLE IF NOT EXISTS product_order (
    id BIGINT AUTO_INCREMENT,
    product_name VARCHAR(64) NOT NULL,
    price BIGINT NOT NULL,
    unit VARCHAR(64) NOT NULL,
    quantity BIGINT NOT NULL,
    product_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY(`product_id`) REFERENCES product(`id`)
)DEFAULT CHARSET=UTF8;
