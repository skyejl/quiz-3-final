package com.twuc.webApp.contracts;

public class GetOrderResponse {
    private Long id;
    private String productName;
    private String unit;
    private Long price;
    private Long quantity;

    public GetOrderResponse(Long id, String productName, String unit, Long price,  Long quantity) {
        this.id = id;
        this.productName = productName;
        this.unit = unit;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getUnit() {
        return unit;
    }

    public Long getPrice() {
        return price;
    }


    public Long getQuantity() {
        return quantity;
    }
}
