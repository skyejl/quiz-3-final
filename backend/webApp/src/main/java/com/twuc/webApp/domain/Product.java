package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 64, nullable = false)
    private String productName;
    @Column(length = 64, nullable = false)
    private String unit;
    @Column(nullable = false)
    private Long price;
    @Column(length = 128, nullable = false)
    private String img;
    @OneToOne(mappedBy = "product")
    private ProductOrder productOrder;

    public Product() {
    }

    public Product(String productName, String unit, Long price, String img) {
        this.productName = productName;
        this.unit = unit;
        this.price = price;
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
