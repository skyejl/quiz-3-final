package com.twuc.webApp.contracts;

public class GetProductsResponse {
    private Long id;
    private String productName;
    private String unit;
    private Long price;
    private String img;

    public GetProductsResponse(Long id, String productName, String unit, Long price, String img) {
        this.id = id;
        this.productName = productName;
        this.unit = unit;
        this.price = price;
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getUnit() {
        return unit;
    }

    public Long getPrice() {
        return price;
    }

    public String getImg() {
        return img;
    }
}
