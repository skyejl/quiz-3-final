package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class ProductOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 64, nullable = false)
    private String productName;
    @Column(length = 64, nullable = false)
    private String unit;
    @Column(nullable = false)
    private Long price;
    @Column(nullable = false)
    private Long quantity;
    @OneToOne
    private Product product;


    public ProductOrder() {
    }

    public ProductOrder(String productName, String unit, Long price, Long quantity, Product product) {
        this.productName = productName;
        this.unit = unit;
        this.price = price;
        this.quantity = quantity;
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
