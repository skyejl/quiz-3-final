package com.twuc.webApp.web;

import com.twuc.webApp.contracts.GetOrderResponse;
import com.twuc.webApp.contracts.GetProductsResponse;
import com.twuc.webApp.contracts.NewProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductOrder;
import com.twuc.webApp.domain.ProductOrderRepository;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProductController {
    private ProductRepository productRepository;
    private ProductOrderRepository orderRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @GetMapping("/products")
    public ResponseEntity getAllProducts() {
        List<GetProductsResponse> products = productRepository.findAll(
                Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(p -> new GetProductsResponse(p.getId(),p.getProductName(),p.getUnit(),p.getPrice(),p.getImg()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(products);
    }
    @PostMapping("/order/{productId}")
    public ResponseEntity createOrderByProduct(@PathVariable Long productId) {
        Long quantity;
        Product product = productRepository.findById(productId).get();
        ProductOrder order = orderRepository.findByProductName(product.getProductName());
        if (!order.getProductName().equals(product.getProductName())) {
            quantity = 1L;
        }
        quantity = order.getQuantity()+ 1L;
        ProductOrder newOrder = new ProductOrder(product.getProductName(), product.getUnit(), product.getPrice(), quantity, product);
        orderRepository.save(newOrder);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/orders")
    public ResponseEntity getAllOrders() {
        List<GetOrderResponse> orders = orderRepository.findAll(
                Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(p -> new GetOrderResponse(p.getId(),p.getProductName(),p.getUnit(),p.getPrice(),p.getQuantity()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(orders);
    }
    @PostMapping("/product")
    public ResponseEntity addNewProduct(@RequestBody NewProductRequest newProductRequest) {
        Product newProduct = new Product(newProductRequest.getProductName(),
                newProductRequest.getUnit(), newProductRequest.getPrice(), newProductRequest.getImg());
        productRepository.save(newProduct);
        return ResponseEntity.ok().build();
    }

}
