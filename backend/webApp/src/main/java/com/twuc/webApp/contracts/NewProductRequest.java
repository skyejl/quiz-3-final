package com.twuc.webApp.contracts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NewProductRequest {
    private String productName;
    private Long price;
    private String img;
    private String unit;
}
