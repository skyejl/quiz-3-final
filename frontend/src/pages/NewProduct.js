import React, { Component } from 'react';
import '../style/new-product.less';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { insertProductItem } from '../actions/products';

class NewProduct extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      formData: {
        productName: '',
        price: 0,
        unit: '',
        img: '',
      },
      disable: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  handleInputChange(event, type) {
    const { formData } = this.state;
    this.setState({
      ...this.state,
      disable: false,
    });
    switch (type) {
      case 'productName':
        this.setState({
          formData: {
            ...formData,
            productName: event.target.value,
          },
        });
        break;
      case 'price':
        this.setState({
          formData: {
            ...formData,
            price: event.target.value,
          },
        });
        break;
      case 'unit':
        this.setState({
          formData: {
            ...formData,
            unit: event.target.value,
          },
        });
        break;
      case 'img':
        this.setState({
          formData: {
            ...formData,
            img: event.target.value,
          },
        });
        break;
      default:
        break;
    }
  }

  submitHandler(event) {
    event.preventDefault();
    const {
      productName, price, unit, img,
    } = this.state.formData;
    if (!productName || !price || !unit || !img) {
      this.setState({
        ...this.state,
        disable: true,
      });
      return;
    }
    if (Number.isNaN(+price)) {
      this.setState({
        ...this.state,
        disable: true,
      });
      return;
    }
    this.props.insertProductItem(this.state.formData);
  }

  render() {
    return (
      <div className="new-product">
        <main className="product-form">
          <h1>添加商品</h1>
          <form onSubmit={this.submitHandler}>
            <label htmlFor="productName" className="required">
                名称：
              <input type="text" name="productName" id="productName" placeholder="名称" onChange={(e) => this.handleInputChange(e, 'productName')} />
            </label>
            <label htmlFor="price" className="required">
                 价格：
              <input type="text" name="price" id="price" placeholder="价格" onChange={(e) => this.handleInputChange(e, 'price')} />
            </label>
            <label htmlFor="unit" className="required">
                 单位：
              <input type="text" name="unit" id="unit" placeholder="单位" onChange={(e) => this.handleInputChange(e, 'unit')} />
            </label>
            <label htmlFor="img" className="required">
                图片：
              <input type="text" name="img" id="img" placeholder="图片" onChange={(e) => this.handleInputChange(e, 'img')} />
            </label>
            <div className="submit">
              <input type="submit" value="提交" disabled={this.state.disable} />
            </div>
          </form>
        </main>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
  insertProductItem,
}, dispatch);
export default connect(null, mapDispatchToProps)(NewProduct);
