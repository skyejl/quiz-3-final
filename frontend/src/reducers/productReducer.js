const initalState = {
  formData: {}
};
const productReducer = (state = initalState, action) => {
  switch (action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state,
        payload: action.payload,
      };
    default:
      return state;
  }
};
export default productReducer;
