export const insertProductItem = (formData) => (dispatch) => {
  fetch('/api/product', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formData), // body data type must match "Content-Type" header
  })
    .then((res) => res.json())
    .then((res) => {
      dispatch({
        type: 'ADD_PRODUCT',
        payload: res.data,
      });
    });
};
