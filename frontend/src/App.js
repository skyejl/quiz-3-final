import React from 'react';
import { BrowserRouter as Router, NavLink } from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router';

import { MdHome, MdShoppingCart, MdAdd } from 'react-icons/md';
import Home from './pages/Home';
import OrderDetail from './pages/OrderDetail';
import NewProduct from './pages/NewProduct';
import './style/home.less';


const App = () => (
  <div>
    <Router>
      <ul className="HomeMenu">
        <li>
          <NavLink activeClassName="active" to="/home">
            <MdHome className="navIcon" />
            商城
          </NavLink>

          <NavLink activeClassName="active" to="/order">
            <MdShoppingCart className="navIcon" />
            订单
          </NavLink>

          <NavLink activeClassName="active" to="/new-product">
            <MdAdd className="navIcon" />
            添加商品
          </NavLink>
        </li>
      </ul>
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/order" component={OrderDetail} />
        <Route exact path="/new-product" component={NewProduct} />
        <Redirect to="/" />
      </Switch>
    </Router>
  </div>
);
export default App;
