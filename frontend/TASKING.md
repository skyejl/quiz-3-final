story 0: Display web navigator

story 1:
AC1:Display Add Product Page
1:Display Blank "Add Product Page".
2:Display inputs and title on page - display input and label
3:Display inputs and title on page - display required field mark on label
4:Display inputs and title on page - display required field mark on label
5:Display title "Add Products"
AC2:Fill Product Information
1:Given invalid input, then the submit button should be disabled.
2:Given valid input, then the submit button should be enabled.
3:When the page is displayed the first time, the submit button should be disabled.
AC3:Submit Product
